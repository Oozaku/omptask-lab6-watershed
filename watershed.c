#include "ift.h"

iftImage *Watershed(iftImage *basins, iftImage *marker, char lambda, iftAdjRel  *A)
{
  iftImage   *label=NULL;
  iftImage   *cost=NULL;
  iftGQueue  *Q=NULL;
  int         i,p,q,l,tmp;
  iftVoxel    u,v;
 
  // Initialization 
  
  label   = iftCreateImage(basins->xsize,basins->ysize,basins->zsize);
  cost    = iftCreateImage(basins->xsize,basins->ysize,basins->zsize);
  if (lambda==0){ // marker is a grayscale image >= basins 
    l = 1;
    Q = iftCreateGQueue(iftMaximumValue(marker)+2,cost->n,cost->val);
    for (p=0; p < basins->n; p++) {
      cost->val[p]  = marker->val[p]+1;
      label->val[p] = IFT_NIL;
      iftInsertGQueue(&Q,p);
    }
  } else { // marker is a n-ary image with labeled seeds
    Q = iftCreateGQueue(iftMaximumValue(basins)+2,cost->n,cost->val);
    for (p=0; p < basins->n; p++) {
      if (marker->val[p] > 0){
	cost->val[p]  = basins->val[p]+1;
      } else {
	cost->val[p] = IFT_INFINITY_INT;
      }
      label->val[p] = IFT_NIL;
      iftInsertGQueue(&Q,p);
    }
  }
    

  // Image Foresting Transform

  while(!iftEmptyGQueue(Q)) {
    p=iftRemoveGQueue(Q);

    if (label->val[p] == IFT_NIL) { // root voxel
      cost->val[p] -= 1;
      if (lambda==0){
	label->val[p]=l; l++;
      } else {
	label->val[p]=marker->val[p];
      }
    }
    
    u = iftGetVoxelCoord(basins,p);

    for (i=1; i < A->n; i++){
      v = iftGetAdjacentVoxel(A,u,i);
      if (iftValidVoxel(basins,v)){	
	q = iftGetVoxelIndex(basins,v);
	if (Q->L.elem[q].color != IFT_BLACK){
	  tmp = iftMax(cost->val[p], basins->val[q]);
	  if (tmp < cost->val[q]){ 
	    iftRemoveGQueueElem(Q,q);
	    label->val[q]  = label->val[p];
	    cost->val[q]   = tmp;
	    iftInsertGQueue(&Q, q);
	  }
	}
      }
    }
  }

  iftDestroyImage(&cost);
  iftDestroyGQueue(&Q);
  iftCopyVoxelSize(basins,label);

  return(label);
}



int main(int argc, char *argv[]) 
{
  iftImage        *img=NULL,*basins=NULL,*marker=NULL,*label=NULL;
  iftAdjRel       *A=NULL;
  timer           *tstart=NULL;

  if (argc!=5)
    iftError("watershed <image.*> <spatial_radius> <lambda (0/1)> <h for lambda 0/seeds.txt for lambda 1>","main");

  tstart = iftTic();

  img = iftReadImageByExt(argv[1]);
  A   = iftCircular(atof(argv[2]));
  basins = iftImageBasins(img,A);
  iftWriteImageByExt(basins,"basins.png");
  iftDestroyAdjRel(&A);
  A   = iftCircular(1.0);
  if (atoi(argv[3])==0){
    marker = iftAddValue(basins,atoi(argv[4]));
    label  = Watershed(basins,marker,0,A);
  } else {
    marker = iftCreateImageFromImage(img);
    iftLabeledSet *S = iftReadSeeds(argv[4],img);
    while (S != NULL) {
      int l;
      int p = iftRemoveLabeledSet(&S,&l);
      marker->val[p] = l+1;
    }
    label  = Watershed(basins,marker,1,A);
  }
  
  iftWriteImageByExt(label,"result.png");
  
  puts("\nDone...");
  puts(iftFormattedTime(iftCompTime(tstart, iftToc())));

  iftDestroyAdjRel(&A);
  iftDestroyImage(&img);  
  iftDestroyImage(&marker);  
  iftDestroyImage(&basins);  
  iftDestroyImage(&label);  



  return(0);
}

